package com.cestlavie.lifeguard;

/**
 * Created by Lee on 2016/4/28.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.net.URL;
import java.util.Scanner;
import java.util.Vector;

/**
 *
 * @author LuoJing-Yuan
 */

public class DataManager extends AsyncTask<String, Void, String> {


    public AsyncResponse delegate = null;
    public boolean Done ;
    public DataManager (AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(final String... params) {
        Done = false ;
        URL mDownloadData = null ;
        String Data = "";
        try {
            if ( params[0].equals("0") ) mDownloadData = new URL("http://opendata.epa.gov.tw/webapi/api/rest/datastore/355000000I-000001/?format=xml");
            else if(  params[0].equals("1") )  mDownloadData = new URL("http://opendata.epa.gov.tw/webapi/api/rest/datastore/355000000I-000001/?format=xml");
            else if(  params[0].equals("2") )  mDownloadData = new URL("http://opendata.cwb.gov.tw/govdownload?dataid=F-C0032-001&authorizationkey=rdec-key-123-45678-011121314&ndctype=XML&ndcnid=6069");
            else if(  params[0].equals("3") )  mDownloadData = new URL("http://opendata.epa.gov.tw/ws/Data/UV/?%24orderby=PublishAgency&%24skip=0&%24top=1000&format=xml");
            else if(  params[0].equals("4") )  mDownloadData = new URL("http://opendata.cwb.gov.tw/govdownload?dataid=F-C0032-001&authorizationkey=rdec-key-123-45678-011121314&ndctype=XML&ndcnid=6069");

            Scanner sc = new Scanner(mDownloadData.openStream());
            while (sc.hasNext()) Data += sc.nextLine() + "\n";

        } catch (Exception e) {
            e.printStackTrace();
        } // catch()
        return Data;
    }


    public String[] GetDataArray(int DataIndex, String Data ) {
        Vector<String> DataArray = new Vector<String>();
        try {

            if (DataIndex == 0) { // PSI
                String[] list = Data.split("<Data>");
                for (int i = 1; i < list.length; i++) {
                    String[] temp = list[i].split("<SiteName>")[1].split("</SiteName>");
                    String site = temp[0];
                    list[i] = temp[1];
                    temp = list[i].split("<County>")[1].split("</County>");
                    String county = temp[0];
                    list[i] = temp[1];
                    temp = list[i].split("<PSI>")[1].split("</PSI>");
                    String PSI = temp[0];
                    list[i] = temp[1];

                    String singleline = "" + i + "\t" + site + "\t" + county + "\t" + PSI;
                    DataArray.add(singleline);
                } // for

            } // if

            else if (DataIndex == 1) {  // PM2.5

                String[] list = Data.split("<Data>");

                for (int i = 1; i < list.length; i++) {
                    String[] temp = list[i].split("<SiteName>")[1].split("</SiteName>");
                    String site = temp[0];
                    list[i] = temp[1];
                    temp = list[i].split("<County>")[1].split("</County>");
                    String county = temp[0];
                    list[i] = temp[1];
                    temp = list[i].split("<PM2.5>")[1].split("</PM2.5>");
                    String PSI = temp[0];
                    list[i] = temp[1];

                    String singleline = "" + i + "\t" + site + "\t" + county + "\t" + PSI;
                    DataArray.add(singleline);
                } // for

            } // else if

            else if (DataIndex == 2) {  // rain

                String[] list = Data.split("<location>");

                for (int i = 1; i < list.length; i++) {
                    String[] temp = list[i].split("<locationName>")[1].split("</locationName>");
                    String loca = temp[0];
                    list[i] = temp[1];

                    temp = list[i].split("<elementName>PoP</elementName>")[1].split("<parameterName>");
                    temp = temp[3].split( "</parameterName>" ) ;
                    String PoP = temp[0];
                    list[i] = temp[1];

                    String singleline = "" + i + "\t" + loca + "\t"  + PoP + "%" ;
                    DataArray.add(singleline);
                } // for

            } // else if

            else if (DataIndex == 3) {  // UV

                String[] list = Data.split("<Data>");

                for (int i = 1; i < list.length; i++) {
                    String[] temp = list[i].split("<SiteName>")[1].split("</SiteName>");
                    String site = temp[0];
                    list[i] = temp[1];

                    temp = list[i].split("<UVI>")[1].split("</UVI>");
                    String UVI= temp[0];
                    list[i] = temp[1];

                    temp = list[i].split("<County>")[1].split("</County>");
                    String county = temp[0];
                    list[i] = temp[1];

                    String singleline = "" + i + "\t" + site + "\t" + county + "\t" + UVI;
                    DataArray.add(singleline);
                } // for

            } // else if

            else if (DataIndex == 4) {  // oC

                String[] list = Data.split("<location>");

                for (int i = 1; i < list.length; i++) {
                    String[] temp = list[i].split("<locationName>")[1].split("</locationName>");
                    String loca = temp[0];
                    list[i] = temp[1];

                    temp = list[i].split("<elementName>MaxT</elementName>")[1].split("<parameterName>");
                    temp = temp[3].split( "</parameterName>" ) ;
                    String degree = temp[0];
                    list[i] = temp[1];

                    String singleline = "" + i + "\t" + loca + "\t"  + degree ;
                    DataArray.add(singleline);
                } // for

            } // else if

        } catch (Exception e) {
            Log.d("luo",e.toString());;
        } // catch
        return DataArray.toArray(new String[DataArray.size()]);
    } // GetData()

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }

    public interface AsyncResponse {
        void processFinish(String output);
    }
    protected void onPreExecute() {}
}