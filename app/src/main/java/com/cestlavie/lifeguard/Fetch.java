package com.cestlavie.lifeguard;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Lee on 2016/4/28.
 */
// fetch current region of user
public class Fetch extends AsyncTask<String, Void, String> {

    public String fetchString;
    public String out ;

    // get country and region name
    public  String getRegion( String inString) {
        // get region
        JSONObject reader = null;
        try {
            reader = new JSONObject(inString);
            JSONArray results = reader.getJSONArray("results");
            JSONObject address_components = results.getJSONObject(0);
            JSONArray in = address_components.getJSONArray("address_components");
            int find = 0 ;
            String country  = new String() ;
            String region = new String() ;
            for(int i = 0 ; i < in.length() ; ++i ) {
                JSONObject temp = in.getJSONObject(i) ;
                if(temp.getString("types").equals("[\"administrative_area_level_1\",\"political\"]")){
                    country = temp.getString("long_name") ;
                    ++find;
                }
                if(temp.getString("types").equals("[\"administrative_area_level_3\",\"political\"]")){
                    region = temp.getString("long_name") ;
                    ++find;
                }
                if(find == 2)
                    break ;
            }

            return country + " \\ " + region ;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "" ;

    }

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    public Fetch(AsyncResponse delegate){
        this.delegate = delegate;
    }
    protected void onPreExecute() {}

    protected String doInBackground(final String... args) {
        try {
            URL url;
            String urlDate="https://maps.googleapis.com/maps/api/geocode/json?latlng=" + args[1] + "," + args[2] + "&language=zh-TW&key=AIzaSyC4pDJZjsnqvSxOO9qIIx8nVpvGEgj86z4";            try {
                //封装访问服务器的地址
                url=new URL(urlDate);
                HttpURLConnection conn ;
                try {
                    //打开对服务器的连接
                    conn=(HttpURLConnection) url.openConnection();
                    //连接服务器
                    conn.connect();
                    Log.d("leec",String.valueOf(conn.getResponseCode()));
                    InputStream is=conn.getInputStream();
                    ByteArrayOutputStream baos=new ByteArrayOutputStream();
                    byte [] buffer=new byte[1024];
                    int len=0;
                    fetchString = null;
                    while((len=is.read(buffer))!=-1){
                        baos.write(buffer, 0, len);
                    }
                    fetchString=baos.toString();
                    baos.close();
                    is.close();
                    // do something whit command of args[0]
                    if(args[0].equals("getRegion"))
                        out = getRegion(fetchString) ;
                    //Log.d("leec", jsonString);



                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return out;
        } catch (Exception e) {
            Log.e("tag", "error", e);
            return out;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }


}
