package com.cestlavie.lifeguard;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HealthFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View healthLayout = inflater.inflate(R.layout.health_layout,
                container, false);
        return healthLayout;
    }

}
