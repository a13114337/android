package com.cestlavie.lifeguard;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, Fetch.AsyncResponse {

    Fetch asyncTask =new Fetch(this);

    public TextView tvRegion = null;

    public Location location;

    private double currentLatitude;

    private double currentLongitude;

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private TextClock mTextClock;

    private View homeLayout;

    String[] values = new String[] { "PSI\t\t\t\t37\t\t\t\t狀況：肺癌機率提升30%",
                                     "PM2.5\t\t\t\t2級\t\t\t\t狀況：沒事兒沒事兒~",
                                     "紫外線\t\t\t\t9級\t\t\t\t狀況：快烤焦拉~",
                                     "氣溫\t\t\t\t32度\t\t\t\t狀況：包你3秒流汗",
                                     "降雨機率\t\t\t\t0%\t\t\t\t狀況：可以出來走走啦" };

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        homeLayout = inflater.inflate(R.layout.home_layout,
                container, false);
        // add spinner to menu button and health button
        final Spinner SpinnerS = (Spinner)homeLayout.findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.setting,

                android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerS.setAdapter(adapter);
        SpinnerS.setSelection(2);
        // manu button
        Button button_menu = (Button) homeLayout.findViewById(R.id.button_menu);

        button_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpinnerS.performClick();
            }
        });
        SpinnerS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            protected Adapter initializedAdapter=null;

            Intent intent = null ;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(initializedAdapter !=parent.getAdapter() ) {
                    initializedAdapter = parent.getAdapter();
                    return;
                }

                switch(position) {
                    case 0:
                        intent = new Intent(getActivity(), WarningActivity.class);
                        break;
                    case 1:
                        intent = new Intent(getActivity(), ItemActivity.class);
                        break;
                    case 2:
                        intent = null;
                        break;
                    default:
                        intent = null;
                }
                if(intent !=null ) {
                    SpinnerS.setSelection(2);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // user health button
        Button button_user = (Button) homeLayout.findViewById(R.id.button_user);
        button_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UserActivity.class) ;
                startActivity(intent);
            }
        });

        mTextClock = (TextClock)homeLayout.findViewById(R.id.textClock);

        mTextClock.setFormat12Hour("yyyy/MM/dd, EEEE, aah:mm");

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        mGoogleApiClient.connect();

        final ListView listview = (ListView) homeLayout.findViewById(R.id.listView2);

        final ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }

        final StableArrayAdapter adapter2 = new StableArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter2);

        return homeLayout;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            Toast.makeText(getActivity(), " Your current location " + currentLatitude + "  " + currentLongitude, Toast.LENGTH_LONG).show();

            Log.d("leec", String.valueOf(currentLatitude) + String.valueOf(currentLongitude));
            // get Region
            asyncTask.delegate = this;
            //execute the async task
            asyncTask.execute("getRegion", String.valueOf(currentLatitude), String.valueOf(currentLongitude));
            //new FetchJSON().execute();
            tvRegion = (TextView) homeLayout.findViewById(R.id.textView11);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("TAG", "Location services suspended. Please reconnect.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }
    @Override
    public void processFinish(String output) {
        tvRegion.setText(output);
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}
