package com.cestlavie.lifeguard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ImageActivity extends Activity {
    GridView grid;
    GridView grid2;
    ImageView imageViewer;
    int[] imageIds = new int[]
            {
                    R.drawable.man2, R.drawable.man3, R.drawable.man4
                    , R.drawable.man5
            };
    int[] imageIds2 = new int[]
            {
                    R.drawable.man
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_layout);
        // 創建一個List物件，List物件的元素是Map
        List<Map<String, Object>> listItems =
                new ArrayList<Map<String, Object>>();
        // 創建一個List物件，List物件的元素是Map
        List<Map<String, Object>> listItems2 =
                new ArrayList<Map<String, Object>>();
        for (int i = 0; i < imageIds.length; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("image", imageIds[i]);
            listItems.add(listItem);
        }
        for (int i = 0; i < imageIds2.length; i++) {
            Map<String, Object> listItem2 = new HashMap<String, Object>();
            listItem2.put("image2", imageIds2[i]);
            listItems2.add(listItem2);
        }
        // 獲取顯示圖片的ImageView
        imageViewer = (ImageView) findViewById(R.id.imageViewer);
        // 創建一個SimpleAdapter
        SimpleAdapter simpleAdapter = new SimpleAdapter(this,
                listItems
                // 使用/layout/cell.xml檔作為介面佈局
                , R.layout.cell, new String[]{"image"},
                new int[]{R.id.image1});
        // 創建一個SimpleAdapter
        SimpleAdapter simpleAdapter2 = new SimpleAdapter(this,
                listItems2
                // 使用/layout/cell.xml檔作為介面佈局
                , R.layout.cell, new String[]{"image2"},
                new int[]{R.id.image1});
        grid = (GridView) findViewById(R.id.grid01);
        grid2 = (GridView) findViewById(R.id.grid02);
        // 為GridView設置Adapter
        grid.setAdapter(simpleAdapter);
        // 為GridView設置Adapter
        grid2.setAdapter(simpleAdapter2);
        // 添加列表項被選中的監聽器
        grid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // 顯示當前被選中的圖片
                imageViewer.setImageResource(imageIds[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // 添加列表項被選中的監聽器
        grid2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // 顯示當前被選中的圖片
                imageViewer.setImageResource(imageIds2[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // 添加列表項被按一下的監聽器
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // 顯示被按一下的圖片
                imageViewer.setImageResource(imageIds[position]);
            }
        });
        // 添加列表項被按一下的監聽器
        grid2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // 顯示被按一下的圖片
                imageViewer.setImageResource(imageIds2[position]);
            }
        });
        Button back_button = (Button) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImageActivity.this, UserActivity.class);
                startActivity(intent);
            }
        });
    }
}

