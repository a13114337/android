package com.cestlavie.lifeguard;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class ItemActivity extends AppCompatActivity {

    private Button okButton;
    private String Carry[] = {"Mask", "Sun block", "Insect repellent"} ;
    private String Memory[] = {"Umbrella"} ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_layout);

        okButton =  (Button) this.findViewById(R.id.ok);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        for(int i = 0; i < Carry.length; i++) {
            CheckBox cb = new CheckBox(getApplicationContext());
            cb.setText(Carry[i]);
            cb.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            cb.setTextColor(Color.BLACK);
            ((LinearLayout)findViewById(R.id.lil)).addView(cb);
        }

        for(int i = 0; i < Memory.length; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setText(Memory[i] + "\n");
            textView.setTextColor(Color.BLACK);
            textView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            ((LinearLayout)findViewById(R.id.lil2)).addView(textView);
        }


    }
}
