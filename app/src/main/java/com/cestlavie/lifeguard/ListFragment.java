package com.cestlavie.lifeguard;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListFragment extends Fragment  implements DataManager.AsyncResponse {

    DataManager  DM  ;
    String[] values = {};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DM  = new DataManager(this) ;
        DM.delegate = this ;
        try {
            values = DM.GetDataArray(3, DM.execute("3").get());
        } catch( Exception e ) { e.printStackTrace();} ;

        View listLayout = inflater.inflate(R.layout.list_layout, container,
                false);

        final ListView listview = (ListView) listLayout.findViewById(R.id.listView);

        final ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }

        final StableArrayAdapter adapter = new StableArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        return listLayout;
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
    @Override
    public void processFinish(String output) {
        Log.d("luo","D:"+output);
        values = DM.GetDataArray(0, output ) ;

    }

}
