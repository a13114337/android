package com.cestlavie.lifeguard;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 項目的主Activity，所有的Fragment都嵌入在這裡。
 *
 * @author guolin
 */
public class MainActivity extends Activity implements OnClickListener {
    /**
     * 用於展示首頁的Fragment
     */
    private HomeFragment homeFragment;

    /**
     * 用於展示地圖顯示的Fragment
     */
    private MapFragment mapFragment;

    /**
     * 用於展示列表顯示的Fragment
     */
    private ListFragment listFragment;

    /**
     * 用於展示重要訊息的Fragment
     */
    private MessageFragment messageFragment;

    /**
     * 首頁介面佈局
     */
    private View homeLayout;

    /**
     * 地圖顯示介面佈局
     */
    private View mapLayout;

    /**
     * 列表顯示介面佈局
     */
    private View listLayout;

    /**
     * 重要訊息介面佈局
     */
    private View messageLayout;

    /**
     * 在Tab佈局上顯示首頁圖示的控制項
     */
    private ImageView homeImage;

    /**
     * 在Tab佈局上顯示地圖顯示圖示的控制項
     */
    private ImageView mapImage;

    /**
     * 在Tab佈局上顯示列表顯示圖示的控制項
     */
    private ImageView listImage;

    /**
     * 在Tab佈局上顯示重要訊息圖示的控制項
     */
    private ImageView messageImage;

    /**
     * 在Tab佈局上顯示首頁標題的控制項
     */
    private TextView homeText;

    /**
     * 在Tab佈局上顯示地圖顯示標題的控制項
     */
    private TextView mapText;

    /**
     * 在Tab佈局上顯示列表顯示標題的控制項
     */
    private TextView listText;

    /**
     * 在Tab佈局上顯示重要訊息標題的控制項
     */
    private TextView messageText;

    /**
     * 用於對Fragment進行管理
     */
    private FragmentManager fragmentManager;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        // 初始化佈局元素
        initViews();
        fragmentManager = getFragmentManager();

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        // 第一次啟動時選中第0個tab
        setTabSelection(0);
    }

    /**
     * 在這裡獲取到每個需要用到的控制項的實例，並給它們設置好必要的點擊事件。
     */
    private void initViews() {
        homeLayout = findViewById(R.id.home_layout);
        mapLayout = findViewById(R.id.map_layout);
        listLayout = findViewById(R.id.list_layout);
        messageLayout = findViewById(R.id.message_layout);
        homeImage = (ImageView) findViewById(R.id.home_image);
        mapImage = (ImageView) findViewById(R.id.map_image);
        listImage = (ImageView) findViewById(R.id.list_image);
        messageImage = (ImageView) findViewById(R.id.message_image);
        homeText = (TextView) findViewById(R.id.home_text);
        mapText = (TextView) findViewById(R.id.map_text);
        listText = (TextView) findViewById(R.id.list_text);
        messageText = (TextView) findViewById(R.id.message_text);
        homeLayout.setOnClickListener(this);
        mapLayout.setOnClickListener(this);
        listLayout.setOnClickListener(this);
        messageLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_layout:
                // 當點擊了首頁tab時，選中第1個tab
                setTabSelection(0);
                break;
            case R.id.map_layout:
                // 當點擊了地圖顯示tab時，選中第2個tab
                setTabSelection(1);
                break;
            case R.id.list_layout:
                // 當點擊了列表顯示tab時，選中第3個tab
                setTabSelection(2);
                break;
            case R.id.message_layout:
                // 當點擊了重要訊息tab時，選中第4個tab
                setTabSelection(3);
                break;
            default:
                break;
        }
    }

    /**
     * 根據傳入的index參數來設置選中的tab頁。
     *
     * @param index 每個tab頁對應的下標。0表示首頁，1表示地圖顯示，2表示列表顯示，3表示重要訊息。
     */
    private void setTabSelection(int index) {
        // 每次選中之前先清楚掉上次的選中狀態
        clearSelection();
        // 開啟一個Fragment事務
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // 先隱藏掉所有的Fragment，以防止有多個Fragment顯示在介面上的情況
        hideFragments(transaction);
        switch (index) {
            case 0:
                // 當點擊了首頁tab時，改變控制項的圖片和文字顏色
                //homeImage.setImageResource(R.drawable.message_selected);
                homeText.setTextColor(Color.WHITE);
                if (homeFragment == null) {
                    // 如果HomeFragment為空，則創建一個並添加到介面上
                    homeFragment = new HomeFragment();
                    transaction.add(R.id.content, homeFragment);
                } else {
                    if (homeFragment.location == null && locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
                        homeFragment = new HomeFragment();
                        transaction.add(R.id.content, homeFragment);
                    }
                    // 如果HomeFragment不為空，則直接將它顯示出來
                    transaction.show(homeFragment);
                }

                if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
                    //Toast.makeText(getBaseContext(), " Please enable GPS ", Toast.LENGTH_LONG).show();

                    new AlertDialog.Builder(this).setTitle("地圖工具").setMessage("您尚未開啟定位服務，要前往設定頁面啟動定位服務嗎？")
                            .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {

                        public void onClick(DialogInterface dialog, int which)
                        {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Toast.makeText(getBaseContext(), "未開啟定位服務，無法使用本工具!!", Toast.LENGTH_SHORT).show();
                        }
                    }).show();

                }

                break;
            case 1:
                // 當點擊了連絡人tab時，改變控制項的圖片和文字顏色
                //mapImage.setImageResource(R.drawable.contacts_selected);
                mapText.setTextColor(Color.WHITE);
                if (mapFragment == null) {
                    // 如果MapFragment為空，則創建一個並添加到介面上
                    mapFragment = new MapFragment();
                    transaction.add(R.id.content, mapFragment);
                } else {
                    if (mapFragment.location == null && locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
                        mapFragment = new MapFragment();
                        transaction.add(R.id.content, mapFragment);
                    }
                    // 如果mapFragment不為空，則直接將它顯示出來
                    transaction.show(mapFragment);
                }

                if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
                    //Toast.makeText(getBaseContext(), " Please enable GPS ", Toast.LENGTH_LONG).show();

                    new AlertDialog.Builder(this).setTitle("地圖工具").setMessage("您尚未開啟定位服務，要前往設定頁面啟動定位服務嗎？")
                            .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {

                        public void onClick(DialogInterface dialog, int which)
                        {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Toast.makeText(getBaseContext(), "未開啟定位服務，無法使用本工具!!", Toast.LENGTH_SHORT).show();
                        }
                    }).show();

                }

                break;
            case 2:
                // 當點擊了動態tab時，改變控制項的圖片和文字顏色
                //listImage.setImageResource(R.drawable.news_selected);
                listText.setTextColor(Color.WHITE);
                if (listFragment == null) {
                    // 如果ListFragment為空，則創建一個並添加到介面上
                    listFragment = new ListFragment();
                    transaction.add(R.id.content, listFragment);
                } else {
                    // 如果ListFragment不為空，則直接將它顯示出來
                    transaction.show(listFragment);
                }
                break;
            case 3:
            default:
                // 當點擊了設置tab時，改變控制項的圖片和文字顏色
                //messageImage.setImageResource(R.drawable.setting_selected);
                messageText.setTextColor(Color.WHITE);
                if (messageFragment == null) {
                    // 如果MessageFragment為空，則創建一個並添加到介面上
                    messageFragment = new MessageFragment();
                    transaction.add(R.id.content, messageFragment);
                } else {
                    // 如果MessageFragment不為空，則直接將它顯示出來
                    transaction.show(messageFragment);
                }
                break;
        }
        transaction.commit();
    }

    /**
     * 清除掉所有的選中狀態。
     */
    private void clearSelection() {
        //homeImage.setImageResource(R.drawable.home);
        homeText.setTextColor(Color.parseColor("#82858b"));
        //mapImage.setImageResource(R.drawable.map);
        mapText.setTextColor(Color.parseColor("#82858b"));
        //listImage.setImageResource(R.drawable.list);
        listText.setTextColor(Color.parseColor("#82858b"));
        //messageImage.setImageResource(R.drawable.message);
        messageText.setTextColor(Color.parseColor("#82858b"));
    }

    /**
     * 將所有的Fragment都置為隱藏狀態。
     *
     * @param transaction 用於對Fragment執行操作的事務
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (homeFragment != null) {
            transaction.hide(homeFragment);
        }
        if (mapFragment != null) {
            transaction.hide(mapFragment);
        }
        if (listFragment != null) {
            transaction.hide(listFragment);
        }
        if (messageFragment != null) {
            transaction.hide(messageFragment);
        }
    }

}

