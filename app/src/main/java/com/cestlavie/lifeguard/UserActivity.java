package com.cestlavie.lifeguard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;

/**
 * 項目的主Activity，所有的Fragment都嵌入在這裡。
 *
 * @author hechaoyue
 */
public class UserActivity extends AppCompatActivity implements OnClickListener {


    /**
     * 用於展示健康的Fragment
     */
    private HealthFragment healthFragment;

    /**
     * 用於展示任務的Fragment
     */
    private TaskFragment taskFragment;

    /**
     * 健康介面佈局
     */
    private View healthLayout;

    /**
     * 任務介面佈局
     */
    private View taskLayout;

    /**
     * 在Tab佈局上健康圖示的控制項
     */
    private ImageView healthImage;

    /**
     * 在Tab佈局上顯示任務圖示的控制項
     */
    private ImageView taskImage;


    /**
     * 在Tab佈局上健康標題的控制項
     */
    private TextView healthText;

    /**
     * 在Tab佈局上顯示任務標題的控制項
     */
    private TextView taskText;

    /**
     * 用於對Fragment進行管理
     */
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_layout);
        // 初始化佈局元素
        initViews();
        fragmentManager = getFragmentManager();
        // 第一次啟動時選中第0個tab
        setTabSelection(0);
    }

    /**
     * 在這裡獲取到每個需要用到的控制項的實例，並給它們設置好必要的點擊事件。
     */
    private void initViews() {
        healthLayout = findViewById(R.id.health_layout);
        taskLayout = findViewById(R.id.task_layout);
        healthImage = (ImageView) findViewById(R.id.health_image);
        taskImage = (ImageView) findViewById(R.id.task_image);
        healthText = (TextView) findViewById(R.id.health_text);
        taskText = (TextView) findViewById(R.id.task_text);
        healthLayout.setOnClickListener(this);
        taskLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.health_layout:
                // 當點擊了健康tab時，選中第1個tab
                setTabSelection(0);
                break;
            case R.id.task_layout:
                // 當點擊了任務tab時，選中第2個tab
                setTabSelection(1);
                break;
            default:
                break;
        }
    }

    /**
     * 根據傳入的index參數來設置選中的tab頁。
     *
     * @param index 每個tab頁對應的下標。0表示健康，1表示任務。
     */
    private void setTabSelection(int index) {
        // 每次選中之前先清楚掉上次的選中狀態
        clearSelection();
        // 開啟一個Fragment事務
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // 先隱藏掉所有的Fragment，以防止有多個Fragment顯示在介面上的情況
        hideFragments(transaction);
        switch (index) {
            case 0:
                // 當點擊了動態tab時，改變控制項的圖片和文字顏色
                //healthImage.setImageResource(R.drawable.news_selected);
                healthText.setTextColor(Color.WHITE);
                if (healthFragment == null) {
                    // 如果HealthFragment為空，則創建一個並添加到介面上
                    healthFragment = new HealthFragment();
                    transaction.add(R.id.content2, healthFragment);
                } else {
                    // 如果HealthFragment不為空，則直接將它顯示出來
                    transaction.show(healthFragment);
                }
                break;
            case 1:
            default:
                // 當點擊了設置tab時，改變控制項的圖片和文字顏色
                //taskImage.setImageResource(R.drawable.setting_selected);
                taskText.setTextColor(Color.WHITE);
                if (taskFragment == null) {
                    // 如果TaskFragment為空，則創建一個並添加到介面上
                    taskFragment = new TaskFragment();
                    transaction.add(R.id.content2, taskFragment);
                } else {
                    // 如果TaskFragment不為空，則直接將它顯示出來
                    transaction.show(taskFragment);
                }
                break;
        }
        transaction.commit();
    }

    /**
     * 清除掉所有的選中狀態。
     */
    private void clearSelection() {
        //healthImage.setImageResource(R.drawable.health);
        healthText.setTextColor(Color.parseColor("#82858b"));
        //taskImage.setImageResource(R.drawable.task);
        taskText.setTextColor(Color.parseColor("#82858b"));
    }

    /**
     * 將所有的Fragment都置為隱藏狀態。
     *
     * @param transaction 用於對Fragment執行操作的事務
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (healthFragment != null)

        {
            transaction.hide(healthFragment);
        }

        if (taskFragment != null)

        {
            transaction.hide(taskFragment);
        }
    }

}


